module gitlab.com/library-dedy/wb-library

go 1.14

require (
	github.com/edganiukov/fcm v0.4.0 // indirect
	github.com/fortytw2/leaktest v1.3.0 // indirect
	github.com/google/go-cmp v0.5.2 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/mailru/easyjson v0.7.6 // indirect
	github.com/olivere/elastic v6.2.35+incompatible
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rs/xid v1.2.1
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.6.1 // indirect
	go.mongodb.org/mongo-driver v1.7.0 // indirect
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	google.golang.org/appengine v1.6.7 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gorm.io/gorm v1.21.8 // indirect
)
