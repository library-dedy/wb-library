package uniqueid

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	"gitlab.com/library-dedy/wb-library/format"
	"gorm.io/gorm"
)

func GenerateProductCode(s string, pad string, length int) string {
	for i := len(s); i < length; i++ {
		s = pad + s
	}
	return s
}

//Move to gorm

func GenerateUniqueID(db *gorm.DB, prefix string, values ...string) string {
	var Code string
	var sequence string
	t := time.Now()
	query := fmt.Sprintf("SELECT %s FROM %s ORDER BY %s DESC LIMIT 1", values[0], values[1], values[0])
	if err := db.Raw(query).Scan(&Code).Error; err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			Code = fmt.Sprintf("%s%s0000000%d", prefix, t.Format("0102006"), 0)
		}
		Code = fmt.Sprintf("%s%s0000000%d", prefix, t.Format("0102006"), 0)
	}
	a := Code[len(Code)-8:]
	seq := format.ToInt64(a)
	sequence = GenerateProductCode(format.ToString(seq+1), "0", 8)
	result := fmt.Sprintf("%s%s%s", prefix, t.Format("0102006"), sequence)
	return result
}
