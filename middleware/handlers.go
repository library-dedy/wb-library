package middleware

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/library-dedy/wb-library/encryption"
	"gitlab.com/library-dedy/wb-library/format"
	"gitlab.com/library-dedy/wb-library/moment"
	"gitlab.com/library-dedy/wb-library/response"
	"gorm.io/gorm"
)

type ConfigMiddleware struct {
	key       string
	Datastore *gorm.DB
}

func NewMiddlewareConfig(key string, db *gorm.DB) *ConfigMiddleware {
	return &ConfigMiddleware{
		key:       key,
		Datastore: db,
	}
}

func (m ConfigMiddleware) HttpLogging(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.RequestURI)
		next.ServeHTTP(w, r)
	})
}

func (m ConfigMiddleware) CorsHeaders(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, PATCH, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, datetime, signature")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("datetime", moment.New().Format("YYYY-MM-DD HH:mm:ss"))

		if r.Method == "OPTIONS" {
			return
		}
		next.ServeHTTP(w, r)
	})
}

func (m ConfigMiddleware) AuthenticationGuard(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var status int64

		Authorization := r.Header.Get("Authorization")
		signature := r.Header.Get("signature")
		datetime := r.Header.Get("datetime")
		query := fmt.Sprintf("SELECT usr_status FROM wb_master.wb_users WHERE usr_token = '%s' LIMIT 1", Authorization)
		//query := db.Raw("SELECT id, name, age FROM users WHERE name = ?", 3).Scan(&result)
		if Authorization == "" {
			json.NewEncoder(w).Encode(response.ResponseBadRequest("Invalid Token"))
			return
		}

		if signature == "" {
			json.NewEncoder(w).Encode(response.ResponseBadRequest("Invalid Signature"))
			return
		}

		if datetime == "" {
			json.NewEncoder(w).Encode(response.ResponseBadRequest("Invalid Datetime"))
			return
		}

		if format.CompareDatetime(datetime) {
			json.NewEncoder(w).Encode(response.ResponseUnauthorized("Invalid Signature"))
			return
		}

		if err := m.Datastore.Raw(query).Scan(&status).Error; err != nil {
			json.NewEncoder(w).Encode(response.ResponseInternalError(err.Error()))
			return
		}

		token := fmt.Sprintf("%s%s", Authorization, datetime)
		ok := encryption.VerifySignature(token, signature, m.key)
		if !ok {
			json.NewEncoder(w).Encode(response.ResponseUnauthorized("Unauthorized!"))
			return
		}

		if status < 1 {
			json.NewEncoder(w).Encode(response.ResponseForbidden("Forbidden!"))
			return
		}

		next.ServeHTTP(w, r)
	})
}

func (m ConfigMiddleware) SetContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		Values := map[string]string{
			"Authorization": r.Header.Get("Authorization"),
			"signature":     r.Header.Get("signature"),
			"datetime":      moment.New().Format("YYYY-MM-DD") + " 23:59:59",
		}
		//context.Set(r, "HeaderInfo", Values)
		ctx := context.Background()
		context.WithValue(ctx, "HeaderInfo", Values)
		next.ServeHTTP(w, r)
	})
}
