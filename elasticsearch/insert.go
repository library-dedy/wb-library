package elasticsearch

import (
	"context"
	"github.com/olivere/elastic"
	"gitlab.com/library-dedy/wb-library/encryption"
)

func InsertData(ES *elastic.Client, index string, types string, data interface{}) (interface{}, error) {
	ctx := context.Background()
	response, err := ES.Index().Index(index).Type(types).Id(encryption.GenerateID()).BodyJson(data).Do(ctx)
	if err != nil {
		return nil, err
	}

	return response, nil
}
