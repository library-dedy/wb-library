package response

import (
	"time"

	"gitlab.com/library-dedy/wb-library/encryption"
)

var (
	start   = time.Now()
	elapsed = time.Now().Sub(start).Seconds()
)

type Response struct {
	ResponseID   string      `json:"response_id"`
	ResponseCode int16       `json:"code"`
	TimeElapsed  float64     `json:"elapsed"`
	Success      bool        `json:"success"`
	Message      string      `json:"message"`
	Pagination   interface{} `json:"pagination,omitempty"`
	Data         interface{} `json:"data,omitempty"`
	Error        interface{} `json:"error,omitempty"`
}

func ResponseSuccess(data interface{}) *Response {
	response := &Response{
		ResponseID:   encryption.GenerateID(),
		ResponseCode: 200,
		TimeElapsed:  elapsed,
		Success:      true,
		Message:      "The request was fullfield",
		Data:         data,
	}

	return response
}

func ResponseNotFound(err interface{}) *Response {
	response := &Response{
		ResponseID:   encryption.GenerateID(),
		ResponseCode: 404,
		TimeElapsed:  elapsed,
		Success:      false,
		Message:      "Error data not found!",
		Error:        err,
	}

	return response
}

func ResponseUnauthorized(err interface{}) *Response {
	response := &Response{
		ResponseID:   encryption.GenerateID(),
		ResponseCode: 401,
		TimeElapsed:  elapsed,
		Success:      false,
		Message:      "Unauthorized",
		Error:        err,
	}

	return response
}

func ResponseForbidden(err interface{}) *Response {
	response := &Response{
		ResponseID:   encryption.GenerateID(),
		ResponseCode: 403,
		TimeElapsed:  elapsed,
		Success:      false,
		Message:      "Forbidden",
		Error:        err,
	}

	return response
}

func ResponseInternalError(err interface{}) *Response {
	response := &Response{
		ResponseID:   encryption.GenerateID(),
		ResponseCode: 500,
		TimeElapsed:  elapsed,
		Success:      false,
		Message:      "Internal server error",
		Error:        err,
	}

	return response
}

func ResponseConflict(err interface{}) *Response {
	response := &Response{
		ResponseID:   encryption.GenerateID(),
		ResponseCode: 409,
		TimeElapsed:  elapsed,
		Success:      false,
		Message:      "Conflict",
		Error:        err,
	}

	return response
}

func ResponseBadRequest(err interface{}) *Response {
	response := &Response{
		ResponseID:   encryption.GenerateID(),
		ResponseCode: 400,
		TimeElapsed:  elapsed,
		Success:      false,
		Message:      "Bad request!",
		Error:        err,
	}

	return response
}

func ResponseMethodNotAllowed(err interface{}) *Response {
	response := &Response{
		ResponseID:   encryption.GenerateID(),
		ResponseCode: 405,
		TimeElapsed:  elapsed,
		Success:      false,
		Message:      "Method not allowed",
		Error:        err,
	}

	return response
}

func ResponseOther(err interface{}, text string) *Response {
	response := &Response{
		ResponseID:   encryption.GenerateID(),
		ResponseCode: 500,
		TimeElapsed:  elapsed,
		Success:      false,
		Message:      text,
		Error:        err,
	}

	return response
}

func ResponseSuccessWithPagination(data interface{}, pagination interface{}) *Response {
	response := &Response{
		ResponseID:   encryption.GenerateID(),
		ResponseCode: 200,
		TimeElapsed:  elapsed,
		Success:      true,
		Message:      "The request was fullfield",
		Pagination:   pagination,
		Data:         data,
	}

	return response
}

type ResponseAPIData struct {
	ResponseCode string      `json:"code"`
	Message      string      `json:"message"`
	Data         interface{} `json:"data,omitempty"`
	Error        interface{} `json:"error,omitempty"`
	Timestamp    string      `json:"timestamp"`
}

func ResponseAPI(code string, message string, data interface{}) *ResponseAPIData {
	response := &ResponseAPIData{
		ResponseCode: code,
		Message:      message,
		Data:         data,
		Timestamp:    time.Now().Format("2006-01-02T15:04:05Z07:00"),
	}

	return response
}

type ResAPI struct {
	Data interface{} `json:"data,omitempty"`
}

func ResponseSuccessAPI(data interface{}) *ResAPI {
	response := &ResAPI{
		Data: data,
	}

	return response
}
func ResponseNFCredentials(data interface{}) *ResAPI {
	response := &ResAPI{
		Data: data,
	}

	return response
}
func ResponseInternalErrorAPI(err interface{}) *Response {
	response := &Response{
		ResponseID:   encryption.GenerateID(),
		ResponseCode: 500,
		TimeElapsed:  elapsed,
		Success:      false,
		Message:      "Internal server error",
		Error:        err,
	}

	return response
}
