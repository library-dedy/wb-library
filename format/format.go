package format

import (
	"strconv"
	"strings"
	"time"
)

func ToString(n interface{}, p ...int) string {
	var t string

	switch n.(type) {
	case bool:
		t = strconv.FormatBool(n.(bool))
	case int:
		t = strconv.Itoa(n.(int))
	case int64:
		t = strconv.FormatInt(n.(int64), 10)
	case float32:
		if len(p) > 0 {
			t = strconv.FormatFloat(float64(n.(float32)), 'f', p[0], 64)
		} else {
			t = strconv.FormatFloat(float64(n.(float32)), 'f', -1, 64)
		}
	case float64:
		if len(p) > 0 {
			t = strconv.FormatFloat(n.(float64), 'f', p[0], 64)
		} else {
			t = strconv.FormatFloat(n.(float64), 'f', -1, 64)
		}
	case byte:
		t = string(n.(byte))
	case []byte:
		t = string(n.([]byte))
	case string:
		t = n.(string)
	}

	return t
}

func ToInt64(params string) int64 {
	number, _ := strconv.ParseInt(params, 10, 64)
	return number
}

func BytesToString(data []byte) string {
	return string(data[:])
}

func FilterPhoneNumber(str string) string {
	getFirstChar := str[:2]
	if getFirstChar == "62" {
		return strings.ReplaceAll(str, str[:2], "62")
	}

	return strings.ReplaceAll(str, str[:1], "62")
}

func ToLower(str string) string {
	return strings.ToLower(str)
}

func ToUpper(str string) string {
	return strings.ToUpper(str)
}

func ToInt(str string) int {
	value, _ := strconv.Atoi(str)
	return value
}

func ToFloat64(str string) float64 {
	value, _ := strconv.ParseFloat(str, 64)
	return value
}

func ToBool(str string) bool {
	value, _ := strconv.ParseBool(str)
	return value
}

func CompareDatetime(datetime string) bool {
	t := time.Now()
	ParseDate := t.Format("2006-01-02") + " 23:59:59"

	createdAt, _ := time.Parse("2006-01-02 15:04:05", datetime)
	expiresAt, _ := time.Parse("2006-01-02 15:04:05", ParseDate)

	diff := expiresAt.Sub(createdAt)

	if diff.Hours() > 0 && diff.Hours() < 24 {
		return true
	}
	return false
}
